//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectMaster.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Czynnosc
    {
        public Czynnosc()
        {
            this.Zadanies = new HashSet<Zadanie>();
        }
    
        public int Id { get; set; }
        public string Nazwa { get; set; }
        public string Klucz { get; set; }
    
        public virtual ICollection<Zadanie> Zadanies { get; set; }
    }
}
