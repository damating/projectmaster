﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectMaster.Model
{
    public class BacklogPelny : Backlog
    {
        public Stan StanP { get; set; }
        public Uzytkownik UzytkownikP { get; set; }

        /// <summary>
        /// Konstruktor tworzący BacklogPelny na podstawie klasy nadrzędnej
        /// </summary>
        /// <param name="backlog">obiekt klasy nadrzędnej</param>
        public BacklogPelny(Backlog backlog)
        {
            this.Id = backlog.Id;
            this.Nazwa = backlog.Nazwa;
            this.TypId = backlog.TypId;
            this.StanId = backlog.StanId;
            this.SprintId = backlog.SprintId;
            this.Opis = backlog.Opis;
            this.PrzypisanyDoId = backlog.PrzypisanyDoId;
            this.Wysilek = backlog.Wysilek;
            this.WartoscBiznesowa = backlog.WartoscBiznesowa;
            this.ProjektId = backlog.ProjektId;
            this.UkonczonoProcent = backlog.UkonczonoProcent;
        }
    }
}
