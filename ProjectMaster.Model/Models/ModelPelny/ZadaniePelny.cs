﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectMaster.Model
{
    public class ZadaniePelny : Zadanie
    {
        public Stan StanP { get; set; }
        public Uzytkownik UzytkownikP { get; set; }

        /// <summary>
        /// Konstruktor tworzący obiekt zadania pełnego
        /// </summary>
        /// <param name="zadanie">Obiekt Zadanie klasy nadrzędnej</param>
        public ZadaniePelny(Zadanie zadanie)
        {
            this.Id = zadanie.Id;
            this.Nazwa = zadanie.Nazwa;
            this.Opis = zadanie.Opis;
            this.StanId = zadanie.StanId;
            this.PrzypisanyDoId = zadanie.PrzypisanyDoId;
            this.Wysilek = zadanie.Wysilek;
            this.CzynnoscId = zadanie.CzynnoscId;
            this.BacklogId = zadanie.BacklogId;
            this.UkonczonoProcent = zadanie.UkonczonoProcent;
        }
    }
}
