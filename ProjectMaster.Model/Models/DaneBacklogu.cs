﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectMaster.Model
{
    public class DaneBacklogu
    {
        public int ProjektId { get; set; }
        public Backlog Backlog { get; set; }
        public List<TypBacklogu> Typy { get; set; }
        public List<Stan> Stany { get; set; }
        public List<Sprint> Sprinty { get; set; }
        public List<Uzytkownik> Uzytkownicy { get; set; }
    }
}
