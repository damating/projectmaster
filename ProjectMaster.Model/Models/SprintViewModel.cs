﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectMaster.Model.Models;

namespace ProjectMaster.Model
{
    public class SprintViewModel
    {
        public Sprint AktualnySprint { get; set; }
        public List<Sprint> Sprinty { get; set; }
        public List<BacklogPelny> Backlogi { get; set; }
        public List<BacklogViewModel> BacklogiTabela { get; set; }
        public Projekt Projekt { get; set; }
        public bool CzyWszystkie { get; set; }
        public bool CzyAdminProjektu { get; set; }
    }
}
        