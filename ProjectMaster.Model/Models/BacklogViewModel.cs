﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectMaster.Model
{
    public class BacklogViewModel
    {
        public BacklogPelny BacklogPelny { get; set; }
        public List<ZadaniePelny> ZadaniaBackloga { get; set; }
    }
}
