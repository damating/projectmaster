﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectMaster.Model
{
    public class RejestrujModel : Uzytkownik
    {
        [Required(ErrorMessage = @"To pole jest wymagane")]
        [StringLength(40, MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = @"Powtórz hasło")]
        [Compare("Haslo", ErrorMessage="Podane hasła nie są zgodne")]
        public string PowtorzoneHaslo { get; set; }
    }
}
