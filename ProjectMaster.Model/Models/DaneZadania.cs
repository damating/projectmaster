﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectMaster.Model
{
    public class DaneZadania
    {
        public Zadanie Zadanie { get; set; }
        public List<Czynnosc> Czynnosci { get; set; }
        public List<Stan> Stany { get; set; }
        public List<Uzytkownik> Uzytkownicy { get; set; }
    }
}
