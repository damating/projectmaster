﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectMaster.Model
{
    public class LogujModel
    {
        [Required(ErrorMessage = @"Pole email jest wymagane")]
        [EmailAddress(ErrorMessage = @"Zły format maila")]
        public string Email { get; set; }

        [Required(ErrorMessage = @"Hasło jest wymagane")]
        [DataType(DataType.Password)]
        [StringLength(40, MinimumLength = 6, ErrorMessage = @"Hasło musi mieć od 6 do 40 znaków długości")]
        [Display(Name = "Hasło")]
        public string Haslo { get; set; }
    }
}
