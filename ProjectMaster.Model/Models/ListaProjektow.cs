﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectMaster.Model
{
    public class ListaProjektow
    {
        public List<Projekt> Projekty { get; set; }
        public string Rola { get; set; }
    }
}
