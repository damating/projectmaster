﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectMaster.Model
{
    public class DaneProjektu
    {
        public Projekt Projekt { get; set; }
        
        public List<Uzytkownik> UzytkownicyWProjekcie { get; set; }

        public List<Sprint> SprintyWProjekcie { get; set; }

        public bool CzyAdminProjektu { get; set; }
    }
}
