﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectMaster.Model
{
    /// <summary>
    /// Klasa reprezentująca tablicę dbo.RolaUzytkownikaProjektu w bazie danych
    /// </summary>
    public partial class RolaUzytkownikaProjektu
    {
        /// <summary>
        /// Zwraca obiekt klasy
        /// </summary>
        /// <param name="u">Uzytkownik</param>
        /// <param name="p">Projekt</param>
        /// <returns></returns>
        public static RolaUzytkownikaProjektu Daj(Uzytkownik u, Projekt p)
        {
            using(var db = new ProjectMasterEntities())
            {
                return (from rup in db.RolaUzytkownikaProjektus where rup.UzytkownikId == u.Id && rup.ProjektId == p.Id select rup).FirstOrDefault();
            }
        }


        private static List<RolaUzytkownikaProjektu> DajRUPoProjektId(int projektId)
        {
            using (var db = new ProjectMasterEntities())
            {
                return (from rup in db.RolaUzytkownikaProjektus where rup.ProjektId == projektId select rup).ToList();
            }
        }

        /// <summary>
        /// Dodanie obiektu do bazy
        /// </summary>
        public void Dodaj()
        {
            using (var db = new ProjectMasterEntities())
            {
                db.RolaUzytkownikaProjektus.Add(this);

                db.SaveChanges();
            }
        }

        /// <summary>
        /// Sprawdza, czy w projekcie istnieje już podany użytkownik
        /// </summary>
        /// <param name="uzytkownik">użytkownik projektu</param>
        /// <param name="projekt">projekt, w którym bierze udział</param>
        /// <returns>true: istnieje; false: nie istnieje</returns>
        public bool CzyIstniejeUzytkownikWProjekcie(Uzytkownik uzytkownik, Projekt projekt)
        {
            using (var db = new ProjectMasterEntities())
            {
                RolaUzytkownikaProjektu rup = Daj(uzytkownik, projekt);

                if (rup != null)
                    return true;
            }

            return false;
        }

        public bool CzyIstniejeScrumMasterProjektu(ref Uzytkownik u)
        {
            List<RolaUzytkownikaProjektu> lista = DajRUPoProjektId(this.ProjektId);
            List<RolaUzytkownikaProjektu> listaScrumMasterow = lista.Where(i => Rola.DajRole(i.RolaId).Klucz == "SMAS").ToList();
            if (listaScrumMasterow.Count() > 0)
            {
                u = Uzytkownik.DajUzytkownika(listaScrumMasterow[0].UzytkownikId);
                return true;
            }
                

            return false;
        }
    }
}
