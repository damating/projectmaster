﻿using ProjectMaster.Model.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectMaster.Model
{
    public partial class Projekt
    {
        public void Dodaj()
        {
            using(var db = new ProjectMasterEntities())
            {
                db.Projekts.Add(new Projekt() { Nazwa = this.Nazwa, Opis = this.Opis, Utworzyl = this.Utworzyl, DataUtworzenia = DateTime.Now });

                db.SaveChanges();
            }
        }

        public static Projekt Daj(int? id)
        {
            using (var db = new ProjectMasterEntities())
            {
                return (from p in db.Projekts where p.Id == id select p).FirstOrDefault();
            }
        }

        public List<Uzytkownik> DajUzytkownikowProjektu(int? projektId)
        {
            List<Uzytkownik> uzytkownicyProjektu = new List<Uzytkownik>();
            using(var db = new ProjectMasterEntities())
            {
                uzytkownicyProjektu.AddRange(from u in db.RolaUzytkownikaProjektus where u.ProjektId == projektId select u.Uzytkownik);
            }

            return uzytkownicyProjektu;
        }

        public List<int> DajIdUzytkownikowProjektu()
        {
            List<int> uzytkownicyProjektu = new List<int>();
            using (var db = new ProjectMasterEntities())
            {
                uzytkownicyProjektu.AddRange(from u in db.RolaUzytkownikaProjektus where u.ProjektId == this.Id select u.Uzytkownik.Id);
            }

            return uzytkownicyProjektu;
        }

        public bool UsunProjekt()
        {
            using(var db = new ProjectMasterEntities())
            {
                Projekt projekt =  (from p in db.Projekts where p.Id == this.Id select p).FirstOrDefault();
                db.Projekts.Remove(projekt);

                try
                {
                    db.SaveChanges();
                }
                catch (DbEntityValidationException dbEx)
                {
                    foreach (var eve in dbEx.EntityValidationErrors)
                    {
                        Log.Dodaj(String.Format("Tabela \"{0}\" w akcji \"{1}\" ma następujące błędy walidacji:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State));

                        foreach (var ve in eve.ValidationErrors)
                        {
                            Log.Dodaj(String.Format("- Kolumna: \"{0}\", Błąd: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage));
                        }
                    }
                }

                return true;
            }
        }

        public bool UsunUzytkownikaZProjektu(Uzytkownik uzytkownik)
        {
            using(var db = new ProjectMasterEntities())
            {
                RolaUzytkownikaProjektu rolaUzytkownikaProjektu = RolaUzytkownikaProjektu.Daj(uzytkownik, this);
                db.Entry(rolaUzytkownikaProjektu).State = System.Data.Entity.EntityState.Deleted;

                try
                {
                    db.SaveChanges();
                }
                catch (DbEntityValidationException dbEx)
                {
                    foreach (var eve in dbEx.EntityValidationErrors)
                    {
                        Log.Dodaj(String.Format("Tabela \"{0}\" w akcji \"{1}\" ma następujące błędy walidacji:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State));

                        foreach (var ve in eve.ValidationErrors)
                        {
                            Log.Dodaj(String.Format("- Kolumna: \"{0}\", Błąd: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage));
                        }
                    }
                }

                return true;
                                                 
            }
        }

        public static DaneProjektu DajDaneProjektu(int id)
        {
            Projekt projekt = Projekt.Daj(id);
            DaneProjektu daneProjektu = new DaneProjektu() { Projekt = projekt, UzytkownicyWProjekcie = projekt.DajUzytkownikowProjektu(id), SprintyWProjekcie = projekt.DajSprinty() };

            return daneProjektu;
        }

        public List<Sprint> DajSprinty()
        {
            List<Sprint> listaSprintow = new List<Sprint>();
            using (var db = new ProjectMasterEntities())
            {
                listaSprintow.AddRange(from s in db.Sprints where s.ProjektId == this.Id select s);
            }
            return listaSprintow;
        }

        public Uzytkownik DajAdministratoraProjektu()
        {
            using(var db = new ProjectMasterEntities())
            {
                return (from i in db.RolaUzytkownikaProjektus where i.ProjektId == this.Id && i.Rola.Klucz == "POWN" select i.Uzytkownik).FirstOrDefault();
            }
        }

        public List<BacklogPelny> DajBacklogiPelne()
        {
            List<BacklogPelny> backlogi = new List<BacklogPelny>();

            using (var db = new ProjectMasterEntities())
            {
                // dodanie do listy wszystkich backlogów danego projektu
                List<Backlog> backlogs = this.DajBacklogi();

                // przejście przez listę
                foreach (var b in backlogs)
                {
                    // rzutowanie z klasy nadrzędnej na podrzędną  
                    BacklogPelny bPelny = new BacklogPelny(b);

                    // dodanie atrybutów
                    bPelny.StanP = Stan.DajStan(b.StanId);
                    bPelny.Uzytkownik = Uzytkownik.DajUzytkownika(b.PrzypisanyDoId);

                    // dodanie do listy
                    backlogi.Add(bPelny);
                }
            }

            return backlogi;
        }

        public List<Backlog> DajBacklogi()
        {
            using (var db=  new ProjectMasterEntities())
            {
                return (from b in db.Backlogs where b.ProjektId == this.Id select b).ToList();
            }
        }
    }
}
