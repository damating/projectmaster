﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectMaster.Model
{
    public partial class Rola
    {   
        public static Rola DajRole(int id)
        {
            using(var db = new ProjectMasterEntities())
            {
                return (from r in db.Rolas where r.Id == id select r).FirstOrDefault();
            }
        }

        public static Rola DajRole(string klucz)
        {
            using(var db = new ProjectMasterEntities())
            {
                return (from r in db.Rolas where r.Klucz == klucz select r).FirstOrDefault();
            }
        }


    }
}
