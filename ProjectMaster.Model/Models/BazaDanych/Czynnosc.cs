﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectMaster.Model
{
    public partial class Czynnosc
    {
        public static List<Czynnosc> Daj()
        {
            using(var db = new ProjectMasterEntities())
            {
                return (from c in db.Czynnoscs select c).ToList();
            }
        }

        public static Czynnosc Daj(int? id)
        {
            using (var db = new ProjectMasterEntities())
            {
                return (from c in db.Czynnoscs where c.Id == id select c).FirstOrDefault();
            }
        }
    }
}
