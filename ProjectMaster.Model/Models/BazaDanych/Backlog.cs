﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectMaster.Model.Models;

namespace ProjectMaster.Model
{
    /// <summary>
    /// Rozszrzenie klasy Backlog
    /// </summary>
    public partial class Backlog
    {
        /// <summary>
        /// Dodanie backlogu do bazy
        /// </summary>
        public void Dodaj()
        {
            using (var db = new ProjectMasterEntities())
            {
                db.Backlogs.Add(this);
                db.SaveChanges();
            }
        }

        /// <summary>
        /// Zwraca backlog o podanym id
        /// </summary>
        /// <param name="id">identyfikator w bazie</param>
        /// <returns>backlog</returns>
        public static Backlog Daj(int? id)
        {
            using(var db = new ProjectMasterEntities())
            {
                return (from b in db.Backlogs where b.Id == id select b).FirstOrDefault();
            }
        }

        /// <summary>
        /// Zwraca wszystkie zadania przypisane do backlogu
        /// </summary>
        /// <returns>Lista zadań backloga</returns>
        public List<Zadanie> DajZadania()
        {
            using (var db = new ProjectMasterEntities())
            {
                return (from z in db.Zadanies where z.BacklogId == this.Id select z).ToList();
            }
        }

        
        /// <summary>
        /// Zwraca wszystkie zadania wraz z pełnymi danymi przypisane do backlogu
        /// </summary>
        /// <returns>Lista zadań backloga</returns>
        public List<ZadaniePelny> DajZadaniaPelne()
        {
            // lista zadań
            List<ZadaniePelny> zadania = new List<ZadaniePelny>();
            using(var db= new ProjectMasterEntities())
            {
                // przypisanie wszystkich zadań backlogu do listy
                List<Zadanie> zadanias = this.DajZadania();
                
                // przejście po wszystkich elementach listy zadań
                foreach(var z in zadanias)
                {
                    // utworzenie obiektu klasy zadaniePelny
                    ZadaniePelny zPelny = new ZadaniePelny(z);

                    // przypisanie pozostałych właściwości
                    zPelny.StanP = Stan.DajStan(z.StanId);
                    zPelny.UzytkownikP = Uzytkownik.DajUzytkownika(z.PrzypisanyDoId);
                    
                    // dodanie do listy
                    zadania.Add(zPelny);
                }
            }
            
            return zadania;
        }

        /// <summary>
        /// Zwraca dane backlogu o podanym Id
        /// </summary>
        /// <param name="id">identyfikator w bazie</param>
        /// <returns>DaneProjektu</returns>
        public static DaneBacklogu DajDaneBacklogu(int id)
        {
            Backlog backlog = Backlog.Daj(id);
            DaneBacklogu daneBacklogu = new DaneBacklogu();

            return daneBacklogu;
        }

        /// <summary>
        /// Zwraca TypBacklogu o podanym identyfikatorze
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public TypBacklogu DajTypBacklogu(int id)
        {
            using(var db = new ProjectMasterEntities())
            {
                return (from t in db.TypBacklogus where t.Id == id select t ).FirstOrDefault();
            }
        }

        public List<Stan> DajStany()
        {
            List<Stan> listaStanow = new List<Stan>();

            using (var db = new ProjectMasterEntities())
            {
                listaStanow.AddRange(from s in db.Stans where s.Typ == "B" select s);
            }

            return listaStanow;
        }

        public List<TypBacklogu> DajTypy()
        {
            List<TypBacklogu> listaTypow = new List<TypBacklogu>();

            using(var db = new ProjectMasterEntities())
            {
                listaTypow.AddRange(from t in db.TypBacklogus select t);
            }

            return listaTypow;
        }

        
    }
}
