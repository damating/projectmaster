﻿using ProjectMaster.Model.Models;
using ProjectMaster.Model.Security;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectMaster.Model
{
    public partial class Uzytkownik
    {
        /// <summary>
        /// Sprawdza czy uzytkownik o podanym hasle istnieje w bazie danych
        /// </summary>
        /// <param name="nazwaUzytkownika"></param>
        /// <param name="haslo"></param>
        /// <returns>True jeśli uzytkownik istnieje i haslo jest poprawne</returns>
        public bool SprawdzUzytkownika()
        {
            MD5Walidator md5 = new MD5Walidator();
            Uzytkownik uzytkownik;

            using(var db = new ProjectMasterEntities())
            {
                uzytkownik = (from u in db.Uzytkowniks where u.Email == Email select u).FirstOrDefault();

                if(uzytkownik != null)
                {
                    return md5.Waliduj(Haslo, uzytkownik.Haslo);

                }
                return false;
                
            }
        }

        /// <summary>
        /// Dodaje użytkownika do bazy danych
        /// </summary>
        /// <param name="imie"></param>
        /// <param name="nazwisko"></param>
        /// <param name="email"></param>
        /// <param name="haslo"></param>
        /// <returns>True, jeżeli użytkownik został poprawnie dodany do bazy</returns>
        public bool DodajUzytkownika()
        {
            MD5Walidator md5 = new MD5Walidator();

            using(var db = new ProjectMasterEntities())
            {
                // dodanie użytkownika do bazy
                //Haslo = md5.DajHashCode(Haslo);

                db.Uzytkowniks.Add(new Uzytkownik() { Imie = Imie, Nazwisko = Nazwisko, Email = Email, Haslo = md5.DajHashCode(Haslo), DataUtworzenia=DateTime.Now});

                try
                {
                    db.SaveChanges();
                }
                catch (DbEntityValidationException dbEx)
                {
                    foreach (var eve in dbEx.EntityValidationErrors)
                    {
                        Log.Dodaj(String.Format("Tabela \"{0}\" w akcji \"{1}\" ma następujące błędy walidacji:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State)); 

                        foreach (var ve in eve.ValidationErrors)
                        {
                            Log.Dodaj(String.Format("- Kolumna: \"{0}\", Błąd: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage));
                        }
                    }
                }
                
            }

            return true;
        }

        /// <summary>
        /// Metoda sprawdzająca czy użytkownik o danym adresie mailowym istnieje w bazie danych
        /// </summary>
        /// <returns>True, jeżeli istnieje użytkownik z podanym mailem</returns>
        public bool CzyIstniejeUzytkownikZMailem()
        {
            Uzytkownik uzytkownik;
            using(var db = new ProjectMasterEntities())
            {
                uzytkownik = (from u in db.Uzytkowniks where u.Email == Email select u).FirstOrDefault();
            }

            if (uzytkownik == null)
                return false;

            return true;
        }

        /// <summary>
        /// Zwraca zalogowanego użytkownika
        /// </summary>
        /// <param name="email">User.Identity.Name</param>
        /// <returns></returns>
        public static Uzytkownik DajUzytkownika(string email)
        {
            using (var db = new ProjectMasterEntities())
            {
                return (from u in db.Uzytkowniks where u.Email == email select u).FirstOrDefault();

            }
        }

        public static int DajIdUzytkownika(string email)
        {
            using (var db = new ProjectMasterEntities())
            {
                return (from u in db.Uzytkowniks where u.Email == email select u).FirstOrDefault().Id;

            }
        }

        public List<Projekt> DajProjekty(string rola)
        {
            List<Projekt> projekty = new List<Projekt>();

            using(var db = new ProjectMasterEntities())
            {
                projekty.AddRange(db.Projekts.SqlQuery("SELECT * FROM F_T_DajProjektyUzytkownika(@p0, @p1)", this.Id, rola));
            }
            //List<Projekt> projekty = new List<Projekt>();
            //using(var db = new ProjectMasterEntities())
            //{
            //    projekty.AddRange(from p in db.Projekts where p.Utworzyl == this.Email select p);
            //}

            return projekty;
        }

        public List<Projekt> DajProjekty()
        {
            List<Projekt> projekty = new List<Projekt>();
            using (var db = new ProjectMasterEntities())
            {
                projekty.AddRange(from p in db.Projekts where p.Utworzyl == this.Email select p);
            }

            return projekty;
        }

        public static List<ListaProjektow> DajListeProjektow(string email)
        {
            List<ListaProjektow> projekty = new List<ListaProjektow>();
            Uzytkownik uzytkownik = Uzytkownik.DajUzytkownika(email);

            projekty.Add(new ListaProjektow() { Rola = "POWN", Projekty = uzytkownik.DajProjekty("POWN") });
            projekty.Add(new ListaProjektow() { Rola = "SMAS", Projekty = uzytkownik.DajProjekty("SMAS") });
            projekty.Add(new ListaProjektow() { Rola = "TEAM", Projekty = uzytkownik.DajProjekty("TEAM") });

            return projekty;
        }

        public static Uzytkownik DajUzytkownika(int? id)
        {
            if (id == null)
                return null;

            if (id == 0)
                return new Uzytkownik() { Id = 0, Imie = " ", Nazwisko = " " };

            using (var db=  new ProjectMasterEntities())
            {
                return (from u in db.Uzytkowniks where u.Id == id select u).FirstOrDefault();
            }
            
        }


        public override string ToString()
        {
            return string.Format("{0} {1}", this.Imie[0], this.Nazwisko);
        }

        public List<Zadanie> DajZadania()
        {
            using(var db = new ProjectMasterEntities())
            {
                return (from z in db.Zadanies where z.PrzypisanyDoId == this.Id select z).ToList();
            }
        }
    }
}
