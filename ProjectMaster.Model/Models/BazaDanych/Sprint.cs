﻿using ProjectMaster.Model.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectMaster.Model
{
    ///
    /// Klasa tabeli dbo.Sprint z bazy danych
    /// </summary>
    public partial class Sprint
    {
        /// <summary>
        /// Zwraca listę backlogów danego sprintu
        /// </summary>
        /// <returns>list backlogó</returns>
        public List<Backlog> DajBacklogi()
        {
            // ref do listy
            List<Backlog> listaBacklogow = new List<Backlog>();
            using (var db = new ProjectMasterEntities())
            {
                // dodanie podlisty wszystkich backlogów danego sprintu
                listaBacklogow.AddRange(from b in db.Backlogs where b.SprintId == this.Id select b);
            }
            return listaBacklogow;
        }

        /// <summary>
        /// Zwraca listę wszystkich backlogów (w postaci pełnej) danego sprintu
        /// </summary>
        /// <returns></returns>
        public List<BacklogPelny> DajBacklogiPelne()
        {
            // pusta list backlogów
            List<BacklogPelny> backlogi = new List<BacklogPelny>();
            using (var db = new ProjectMasterEntities())
            {
                // dodanie do listy wszystkich backlogów danego projektu
                List<Backlog> backlogs = this.DajBacklogi();

                // przejście przez listę
                foreach(var b in backlogs)
                {
                    // rzutowanie z klasy nadrzędnej na podrzędną  
                    BacklogPelny bPelny = new BacklogPelny(b);

                    // dodanie atrybutów
                    bPelny.StanP = Stan.DajStan(b.StanId);
                    bPelny.Uzytkownik = Uzytkownik.DajUzytkownika(b.PrzypisanyDoId);
                    
                    // dodanie do listy
                    backlogi.Add(bPelny);
                }
            }

            return backlogi;
        }

        /// <summary>
        /// Zwaraca obiekt sprintu o podanym id
        /// </summary>
        /// <param name="id">identyfikator w bazie</param>
        /// <returns>obiek z bazy</returns>
        public static Sprint Daj(int ?id)
        {
            using (var db = new ProjectMasterEntities())
            {
                return (from s in db.Sprints where s.Id == id select s).FirstOrDefault();
            }
        }

        /// <summary>
        /// Zwraca liczbę sprintów, jakie istnieją w projekcie
        /// </summary>
        /// <param name="projektId">Id projektu</param>
        /// <returns>Int: liczba sprintów</returns>
        public static int DajLiczbeSprintowProjektu(int projektId)
        {
            using(var db = new ProjectMasterEntities())
            {
                return (from s in db.Sprints where s.ProjektId == projektId select s).Count();
            }
        }

        /// <summary>
        /// Dodaje obiekt Sprint do bazy danych
        /// </summary>
        public void Dodaj()
        {
            using(var db = new ProjectMasterEntities())
            {
                db.Sprints.Add(this);

                db.SaveChanges();
            }
        }

    }
}
