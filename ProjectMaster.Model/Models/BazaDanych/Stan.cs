﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectMaster.Model
{
    public partial class Stan
    {
        public static  Stan DajStan(int id)
        {
            using(var db=  new ProjectMasterEntities())
            {
                return (from s in db.Stans where s.Id == id select s).FirstOrDefault();

            }
        }

        public static List<Stan> DajStanyBacklogow()
        {
            List<Stan> listaStanow = new List<Stan>();

            using(var db = new ProjectMasterEntities())
            {
                listaStanow.AddRange(from s in db.Stans where s.Typ == "B" select s);
            }

            return listaStanow;
        }

        public static List<Stan> DajStanyZadan()
        {
            List<Stan> listaStanow = new List<Stan>();

            using (var db = new ProjectMasterEntities())
            {
                listaStanow.AddRange(from s in db.Stans where s.Typ == "Z" select s);
            }

            return listaStanow;
        }
    }
}
