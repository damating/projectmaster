﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectMaster.Model
{
    /// <summary>
    /// Klasa tabeli dbo.Zadanie z bazy danych
    /// </summary>
    public partial class Zadanie
    {
        /// <summary>
        /// Zwraca obiekt zadania o podanym id
        /// </summary>
        /// <param name="id">identyfikator w bazie</param>
        /// <returns>obiekt klasy Zadanie</returns>
        public static Zadanie Daj(int id)
        {
            using(var db = new ProjectMasterEntities())
            {
                return (from z in db.Zadanies where z.Id == id select z).FirstOrDefault();
            }
        }

        public static List<Stan> DajStany()
        {
            List<Stan> listaStanow = new List<Stan>();

            using (var db = new ProjectMasterEntities())
            {
                listaStanow.AddRange(from s in db.Stans where s.Typ == "Z" select s);
            }

            return listaStanow;
        }

        public static List<Czynnosc> DajCzynnosci()
        {
            List<Czynnosc> listaCzynnosci = new List<Czynnosc>();

            using (var db = new ProjectMasterEntities())
            {
                listaCzynnosci.AddRange(from c in db.Czynnoscs select c);
            }

            return listaCzynnosci;
        }

        public void Dodaj()
        {
            using (var db = new ProjectMasterEntities())
            {
                db.Zadanies.Add(this);
                db.SaveChanges();
            }
        }
    }
}
