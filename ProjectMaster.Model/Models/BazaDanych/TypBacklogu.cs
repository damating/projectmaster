﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectMaster.Model
{
    public partial class TypBacklogu
    {
        public static List<TypBacklogu> DajTypyBacklogow()
        {
            List<TypBacklogu> listaBacklogow = new List<TypBacklogu>();

            using (var db = new ProjectMasterEntities())
            {
                listaBacklogow = (from t in db.TypBacklogus select t).ToList();
            }

            return listaBacklogow;
        }

        public static TypBacklogu Daj(int? id)
        {
            using (var db = new ProjectMasterEntities())
            {
                return (from t in db.TypBacklogus where t.Id == id select t).FirstOrDefault();
            }
        }
    }
}
