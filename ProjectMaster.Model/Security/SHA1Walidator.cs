﻿using ProjectMaster.Model.Security.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ProjectMaster.Model.Security
{
    /// <summary>
    /// Klasa obsługująca walidację SHA1
    /// </summary>
    public class SHA1Walidator : IWalidator
    {
        /// <summary>
        /// Funkcja zwraca hashCode po walidacji
        /// </summary>
        /// <param name="zrodlo">tekst do walidacji</param>
        /// <returns>hashCode po walidacji</returns>
        public string DajHashCode(string zrodlo)
        {
            // Konwersja źródłowego stringa do tablicy bajtów
            byte[] dane = Encoding.UTF8.GetBytes(zrodlo);
            
            SHA1CryptoServiceProvider sha1 = new SHA1CryptoServiceProvider();

            // zwrócenie shashowanego stringa
            return BitConverter.ToString(sha1.ComputeHash(dane)).Replace("-", "").ToLower();
        }

        /// <summary>
        /// Funckja sprawdzająca poprawność walidacji hasha i stringa.
        /// </summary>
        /// <param name="zrodlo">tekst do walidacji</param>
        /// <param name="hash">hashCode po walidacji</param>
        /// <returns>Poprawność walidacji</returns>
        public bool Waliduj(string zrodlo, string hash)
        {
            // Hashowanie źródła
            string hashZrodla = DajHashCode(zrodlo);

            // Utworzenie instancji StringComparer i porównanie stringów
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;
            if (comparer.Compare(hashZrodla.ToLower(), hash.ToLower()) == 0)
            {
                return true;
            }

            return false;
        }
    }
}
