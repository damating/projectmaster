﻿using ProjectMaster.Model.Security.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ProjectMaster.Model.Security
{
    /// <summary>
    /// Klasa obsługująca walidację MD5
    /// </summary>
    public class MD5Walidator : IWalidator
    {
        /// <summary>
        /// Funkcja zwraca hashCode po walidacji
        /// </summary>
        /// <param name="zrodlo">tekst do walidacji</param>
        /// <returns>hashCode po walidacji</returns>
        public string DajHashCode(string zrodlo)
        {
            using (MD5 md5 = MD5.Create())
            {
                // Konwersja źródłowego stringa do tablicy bajtów i obliczenie hasha
                byte[] dane = md5.ComputeHash(Encoding.UTF8.GetBytes(zrodlo));

                // Stworzenie instancji StringBuilder do przechowania kolekcji bajtów
                StringBuilder sb = new StringBuilder();

                // Przejście przez tablicę zhashowanych danych i format każdego elementu do szestanstowej wartości
                // Dodanie elementu do stringbuildera
                foreach(var i in dane)
                {
                    sb.Append(i.ToString("x2"));
                }

                // zwrócenie stringa
                return sb.ToString().ToLower();
            }
        }

        /// <summary>
        /// Funckja sprawdzająca poprawność walidacji hasha i stringa.
        /// </summary>
        /// <param name="zrodlo">tekst do walidacji</param>
        /// <param name="hash">hashCode po walidacji</param>
        /// <returns>Poprawność walidacji</returns>
        public bool Waliduj(string zrodlo, string hash)
        {
            using(MD5 md5 = MD5.Create())
            {
                // Hashowanie źródła
                string hashZrodla = DajHashCode(zrodlo);
                
                // Utworzenie instancji StringComparer i porównanie stringów
                StringComparer comparer = StringComparer.OrdinalIgnoreCase;
                if(comparer.Compare(hashZrodla.ToLower(), hash.ToLower()) == 0)
                {
                    return true;
                }

                return false;
            }
        }
    }
}
