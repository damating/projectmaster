﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectMaster.Model
{
    public class Zabezpieczenia
    {
        /// <summary>
        /// Pozawala użytkownikowi przejść do projektu.
        /// </summary>
        /// <param name="idUzytkownika"></param>
        /// <param name="idProjektu"></param>
        /// <returns>true: jeżeli użytkownik jest uprawniony do projektu</returns>
        public static bool UzytkownikProjektu(string mail, int idProjektu)
        {
            Uzytkownik uzytkownik = Uzytkownik.DajUzytkownika(mail);
            Projekt projekt = Projekt.Daj(idProjektu);

            List<int> uzytkownicyProjektu = projekt.DajIdUzytkownikowProjektu();

            if (!uzytkownicyProjektu.Contains(uzytkownik.Id))
                return false;

            return true;
        }

        public static bool CzyAdminProjektu(string mail, int idProjektu)
        {
            Uzytkownik uzytkownik = Uzytkownik.DajUzytkownika(mail);
            Projekt projekt = Projekt.Daj(idProjektu);

            Uzytkownik administrator = projekt.DajAdministratoraProjektu();

            if (uzytkownik.Id == administrator.Id)
                return true;

            return false;
        }
    }
}
