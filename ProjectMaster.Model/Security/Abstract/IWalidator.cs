﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectMaster.Model.Security.Abstract
{
    public interface IWalidator
    {
        string DajHashCode(string zrodlo);
        bool Waliduj(string zrodlo, string hash);
    }
}
