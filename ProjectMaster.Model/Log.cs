﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectMaster.Model.Models
{
    public class Log
    {
        public static void Dodaj(string log)
        {
            DateTime DataLogowania = DateTime.Now;
            using (StreamWriter sw = new StreamWriter(OdczytajSciezke(), true))
            {
                sw.WriteLine(string.Format("[{0}]: {1}", DataLogowania.ToString(), log));
            }
        }


        private static string OdczytajSciezke()
        {
            return System.Configuration.ConfigurationManager.AppSettings["sciezkaLogowania"];
        }
    }
}
