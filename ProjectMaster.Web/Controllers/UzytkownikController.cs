﻿using ProjectMaster.Model;
using ProjectMaster.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ProjectMaster.Web.Controllers
{
    public class UzytkownikController : Controller
    {
        /*
         * GET: /Uzytkownik/Index
         */
        [Authorize]
        public ActionResult Index()
        {
            //List<ListaProjektow> projekty = new List<ListaProjektow>();
            //Uzytkownik uzytkownik = Uzytkownik.DajZalogowanegoUzytkownika(User.Identity.Name);

            //projekty.Add(new ListaProjektow() { Rola = "POWN", Projekty = uzytkownik.DajProjekty("POWN") });
            //projekty.Add(new ListaProjektow(4) { Rola = "SMAS", Projekty = uzytkownik.DajProjekty("SMAS") });
            //projekty.Add(new ListaProjektow() { Rola = "TEAM", Projekty = uzytkownik.DajProjekty("TEAM") });
            List<ListaProjektow> projekt = Uzytkownik.DajListeProjektow(User.Identity.Name);

            return View(Uzytkownik.DajListeProjektow(User.Identity.Name));
        }


        /* 
         * GET: /Uzytkownik/Loguj 
         */
        [HttpGet]
        public ActionResult Loguj()
        {
            return View();
        }

        /*
         * POST: /Uzytkownik/Loguj
         */
        [HttpPost]
        public ActionResult Loguj(LogujModel logujModel)
        {
            // weryfiakacja poprawnośni ModelState
            if(ModelState.IsValid)
            {
                Uzytkownik uzytkownik = new Uzytkownik() { Email = logujModel.Email, Haslo = logujModel.Haslo };

                if (uzytkownik.SprawdzUzytkownika())
                {
                    FormsAuthentication.SetAuthCookie(uzytkownik.Email, true);
                    return RedirectToAction("Index", "Uzytkownik");
                }
                else
                {
                    ModelState.AddModelError("", "błędne dane logowania");
                }
            }
            return View();
        }

        /*
         * GET: Uzytkownik/Rejestruj
         */
        [HttpGet]
        public ActionResult Rejestruj()
        {
            return View();
        }

        /*
         * POST: Uzytkownik/Rejestruj
         */
        [HttpPost]
        public ActionResult Rejestruj(RejestrujModel uzytkownik)
        {
            // weryfikacja ModelState
            if(ModelState.IsValid)
            {
                if (uzytkownik.CzyIstniejeUzytkownikZMailem())
                {
                    ModelState.AddModelError("", "użytkownik o danym adresie mailowym istnieje w bazie danych!");
                }
                else
                {
                    //dodanie użytkownika do bazy
                    uzytkownik.DodajUzytkownika();
                    return RedirectToAction("Index", "Glowna");
                }
            }

            return View(uzytkownik);
        }

        /*
         * GET: Uzytkownik/Wyloguj
         */
        public ActionResult Wyloguj()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Glowna");
        }

        /*
         * GET: Uzytkownik/Zarzadzaj
         */
        [Authorize]
        public ActionResult Zarzadzaj()
        {
            return View();
        }
    }
}