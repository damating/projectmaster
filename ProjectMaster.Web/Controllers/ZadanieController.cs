﻿using ProjectMaster.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectMaster.Web.Controllers
{
    [Authorize]
    public class ZadanieController : Controller
    {
        // GET: Zadanie
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ViewDodajEdytuj(int id, int? idBacklogu)
        {
            Zadanie zadanie = new Zadanie();

            DaneZadania zadaniePelne = new DaneZadania();
            zadaniePelne.Zadanie = new Zadanie();

            // edycja
            if (idBacklogu == null)
            {
                zadaniePelne.Zadanie = Zadanie.Daj(id);
                idBacklogu = zadaniePelne.Zadanie.BacklogId;

            }

            Backlog backlog = Backlog.Daj(idBacklogu);
            zadaniePelne.Zadanie.BacklogId = backlog.Id;
            int projektId = backlog.ProjektId;
            Projekt aktualnyProjekt = Projekt.Daj(projektId);

            List<Uzytkownik> listaUzytkownikow = aktualnyProjekt.DajUzytkownikowProjektu(projektId);
            listaUzytkownikow.Insert(0, new Uzytkownik() { Email = "     " });
            listaUzytkownikow.Insert(0, Uzytkownik.DajUzytkownika(zadaniePelne.Zadanie.PrzypisanyDoId));
            listaUzytkownikow.Remove(null);
            zadaniePelne.Uzytkownicy = listaUzytkownikow;
            
            zadaniePelne.Stany = Stan.DajStanyZadan();
            zadaniePelne.Stany.Insert(0, Stan.DajStan(zadaniePelne.Zadanie.StanId));
            zadaniePelne.Stany.Remove(null);

            zadaniePelne.Czynnosci = Czynnosc.Daj();
            zadaniePelne.Czynnosci.Insert(0, Czynnosc.Daj(zadaniePelne.Zadanie.CzynnoscId));
            zadaniePelne.Czynnosci.Remove(null);

            return PartialView("_DodajEdytuj", zadaniePelne);
        }

        [HttpPost]
        public ActionResult DodajEdytuj(Zadanie zadanie, string stan, string czynnosc, string uzytkownik)
        {
            zadanie.StanId = Int32.Parse(stan);
            zadanie.CzynnoscId = Int32.Parse(czynnosc);
            zadanie.PrzypisanyDoId = Int32.Parse(uzytkownik);
            int backlogId = zadanie.BacklogId;
            Backlog backlog = Backlog.Daj(backlogId);
            int? idAktualnegoSprintu = backlog.SprintId;

            if (ModelState.IsValid)
            {
                using (var db = new ProjectMasterEntities())
                {
                    if (string.IsNullOrEmpty(zadanie.Opis))
                        zadanie.Opis = " ";

                    if (zadanie.Id != 0)
                        db.Entry<Zadanie>(zadanie).State = EntityState.Modified;
                    else
                        db.Zadanies.Add(zadanie);
                    db.SaveChanges();
                }
            }

            if(idAktualnegoSprintu == 0)
            {
                // pobranie projektu
                Projekt projekt = Projekt.Daj(backlog.ProjektId);

                // przykładowy sprint projektu
                Sprint spr = projekt.DajSprinty()[0];

                // przekieruj do wszystkich backlogów projektu
                return RedirectToAction("Index", "Sprint", new { @id = spr.Id, @idProjektu = projekt.Id });
            }

            return RedirectToAction("Index", "Sprint", new { @id = idAktualnegoSprintu });

        }
        
        public ActionResult Lista()
        {
            Uzytkownik uzytkownik = Uzytkownik.DajUzytkownika(User.Identity.Name);
            List<Zadanie> zadania = uzytkownik.DajZadania();

            return View(zadania);
        }
    }
}