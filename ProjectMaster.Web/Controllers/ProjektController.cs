﻿using ProjectMaster.Model;
using ProjectMaster.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectMaster.Web.Controllers
{
    [Authorize]
    public class ProjektController : Controller
    {
        /*
         * GET: Projekt/1
         */
        
        public ActionResult Index(int id)
        {
            if (!Zabezpieczenia.UzytkownikProjektu(User.Identity.Name, id))
                return View("NoPermission");

            DaneProjektu dane = Projekt.DajDaneProjektu(id);
            dane.CzyAdminProjektu = false;

            if (Zabezpieczenia.CzyAdminProjektu(User.Identity.Name, id))
                dane.CzyAdminProjektu = true;

            return View(dane);
        }

        [HttpPost]
        public ActionResult Index(int id, string mail, string rolaKlucz)
        {
            DaneProjektu dane = Projekt.DajDaneProjektu(id);
            dane.CzyAdminProjektu = false;

            if (Zabezpieczenia.CzyAdminProjektu(User.Identity.Name, id))
                dane.CzyAdminProjektu = true;

            if(ModelState.IsValid)
            {
                // przypisz role i projekt - bez walidacji (zawsze będą przesłane dobre dane)
                Rola rola = Rola.DajRole(rolaKlucz);
                Projekt projekt = Projekt.Daj(id);

                // waliduj pusty email
                if (string.IsNullOrEmpty(mail))
                {
                    ModelState.AddModelError("", "Brak adresu mail");
                    return View(dane);
                }

                // jak użytkownik nie jest pusty to go przypisz
                Uzytkownik uzytkownik = Uzytkownik.DajUzytkownika(mail);

                // waliduj istnienie użytkownika w bazie danych
                if (uzytkownik == null)
                {
                    ModelState.AddModelError("", "W bazie nie istnieje użytkownik o danym adresie email");
                    return View(dane);
                }

                

                RolaUzytkownikaProjektu rolaUzytkownikaProjektu = new RolaUzytkownikaProjektu() { ProjektId = projekt.Id, UzytkownikId = uzytkownik.Id, RolaId = rola.Id };

                Uzytkownik scrumMaster = new Uzytkownik();
                if(rolaUzytkownikaProjektu.RolaId == 2 && rolaUzytkownikaProjektu.CzyIstniejeScrumMasterProjektu(ref scrumMaster))
                {
                    ModelState.AddModelError("", string.Format("Istnieje scrum master w tym projekcie- usuń użytkownika {0} {1}, aby dodać nowego ScrumMastera", scrumMaster.Imie[0], scrumMaster.Nazwisko));
                    return View(Projekt.DajDaneProjektu(id));
                }
                
                rolaUzytkownikaProjektu.Dodaj();
                    

            }

            dane = Projekt.DajDaneProjektu(id);
            dane.CzyAdminProjektu = false;

            if (Zabezpieczenia.CzyAdminProjektu(User.Identity.Name, id))
                dane.CzyAdminProjektu = true;

            return View(dane);
           // return View();
        }

        [HttpGet]
        public ActionResult Dodaj()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Dodaj(Projekt projekt)
        {
            if(ModelState.IsValid)
            {
                projekt.Utworzyl = User.Identity.Name;
                projekt.Dodaj();

                return RedirectToAction("Index", "Uzytkownik");
            }
            return View(projekt);
        }

        public ActionResult Usun(int id)
        {
            if (!Zabezpieczenia.CzyAdminProjektu(User.Identity.Name, id))
                return View("NoPermission");

            Projekt p = Projekt.Daj(id);
            p.UsunProjekt();

            return RedirectToAction("Index", "Uzytkownik");
        }
            
        public ActionResult UsunUzytkownika(int idProjektu, int idUzytkownika)
        {
            if (!Zabezpieczenia.CzyAdminProjektu(User.Identity.Name, idProjektu))
                return View("NoPermission");

            Projekt p = Projekt.Daj(idProjektu);
            Uzytkownik u = Uzytkownik.DajUzytkownika(idUzytkownika);

            p.UsunUzytkownikaZProjektu(u);

            return RedirectToAction("Index", "Uzytkownik");
        }
      
    }
}