﻿using ProjectMaster.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectMaster.Web.Controllers
{
    public class GlownaController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            // jeżeli użytkownik jest zalogowany, to przekieruj na widok projektów
            if (User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Uzytkownik");

            ViewBag.Message = "Strona startowa";
            return View();
        }
    }
}