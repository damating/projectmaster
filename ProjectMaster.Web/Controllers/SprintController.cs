﻿using ProjectMaster.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectMaster.Web.Controllers
{
    public class SprintController : Controller
    {
        // GET: Sprint
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">Id jednego ze sprinów</param>
        /// <returns></returns>
        public ActionResult Index(int id, int? idProjektu)
        {
            // utworzenie instancji obiektu
            SprintViewModel sprintViewModel = new SprintViewModel();

            // pobranie sprintu
            Sprint sprint = Sprint.Daj(id);
            sprintViewModel.AktualnySprint = sprint;

            // pobranie projektu, w którym jest dany sprint
            Projekt projekt = Projekt.Daj(sprint.ProjektId);
            sprintViewModel.Projekt = projekt;

            // pobranie sprintów danego projektu
            List<Sprint> sprinty = projekt.DajSprinty();
            // przypisanie do wysyłanego modelu
            sprintViewModel.Sprinty = sprinty;

            List<BacklogPelny> backlogi = new List<BacklogPelny>();

            if(idProjektu != null)
            {
                sprintViewModel.CzyWszystkie = true;
                backlogi = projekt.DajBacklogiPelne();
            }
            else
            {
                sprintViewModel.CzyWszystkie = false;
                // pobranie backlogów danego sprintu
                backlogi = sprint.DajBacklogiPelne();
            }
            

            
            sprintViewModel.Backlogi = backlogi;

            sprintViewModel.BacklogiTabela = new List<BacklogViewModel>();
            foreach(var b in backlogi)
            {
                sprintViewModel.BacklogiTabela.Add(new BacklogViewModel()
                    {
                        BacklogPelny = b,
                        ZadaniaBackloga = b.DajZadaniaPelne()
                    });
            }

            // przypisanie aktualnego sprintu

            if (!Zabezpieczenia.UzytkownikProjektu(User.Identity.Name, projekt.Id))
                return View("NoPermission");

            if (Zabezpieczenia.CzyAdminProjektu(User.Identity.Name, projekt.Id))
                sprintViewModel.CzyAdminProjektu = true;

            return View(sprintViewModel);
        }

        public ActionResult Dodaj(int id)
        {
            if (!Zabezpieczenia.CzyAdminProjektu(User.Identity.Name, id))
                return View("NoPermission");

            Sprint sprint = new Sprint()
            {
                Nazwa = string.Format("Sprint{0}", (Sprint.DajLiczbeSprintowProjektu(id) + 1).ToString()),
                ProjektId = id,
                DataUtworzenia = DateTime.Now
            };

            sprint.Dodaj();

            return RedirectToAction("Index", "Sprint", new { @id = sprint.Id });
        }

        public ActionResult ViewEdytuj(int id)
        {
            Sprint sprint = Sprint.Daj(id);
            return PartialView("_Edytuj", sprint);
        }

        [HttpPost]
        public ActionResult Edytuj(Sprint sprint)
        {
            if (ModelState.IsValid)
            {
                using (var db = new ProjectMasterEntities())
                {
                    db.Entry<Sprint>(sprint).State = EntityState.Modified;

                    db.SaveChanges();
                }
            }

            return RedirectToAction("Index", "Sprint", new { @id = sprint.Id });
        }

        //public ActionResult Index(int id, int project)
        //{
        //    return View();
        //}

        //public PartialViewResult Menu(int id)
        //{
        //    Projekt projekt = Projekt.DajProjekt(id);
        //    //SprintViewModel sp = (SprintViewModel)sprint;
        //    //sp.Backlogi = sprint.DajBacklogi();

        //    ProjectViewModel viewModel = new ProjectViewModel()
        //    {
        //        Sprinty = projekt.DajSprinty()
        //    };
        //    return PartialView(viewModel);
        //}

        public ActionResult ViewWyswietlTablice(int id)
        {
            Sprint sprint = Sprint.Daj(id);
            List<Backlog> Backlogi = sprint.DajBacklogi();
            List<Zadanie> zadania = new List<Zadanie>();
            foreach(var b in Backlogi)
            {
                zadania.AddRange(b.DajZadania());
            }

            //List<Zadanie> zadaniaTODO = new List<Zadanie>();
            //foreach(var z in zadania)
            //{
            //    if(z.StanId == Stan.DajStanyZadan())
            //}

            return PartialView("_WyswietlTablice",zadania);
        }

        [HttpPost]
        public ActionResult WyswietlTablice()
        {
            return View("Index");
        }
    }
}