﻿using ProjectMaster.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectMaster.Web.Controllers
{
    public class BacklogController : Controller
    {
        public ActionResult ViewDodajEdytuj(int id, int? idProjektu)
        {
            // nowa instancja backlogu
            Backlog backlog = new Backlog();

            // pobranie projektu
            Projekt aktualnyProjekt;

            List<TypBacklogu> typyBacklogu = new List<TypBacklogu>();
            DaneBacklogu backlogPelny = new DaneBacklogu();
           
            // przy dodawaniu
            if (idProjektu != null)
            {
                // pobranie aktualnego projektu
                aktualnyProjekt = Projekt.Daj(idProjektu);
                backlogPelny.ProjektId = aktualnyProjekt.Id;
            }
            // przy edycji
            else
            {
                backlogPelny.Backlog = Backlog.Daj(id);
                idProjektu = backlogPelny.Backlog.ProjektId;
                aktualnyProjekt = Projekt.Daj(idProjektu);
            }

            // pobranie listy sprintów
            List<Sprint> listaSprintow = aktualnyProjekt.DajSprinty();
            listaSprintow.Insert(0, new Sprint() { Nazwa = "      " });
            if(backlogPelny.Backlog != null)
                listaSprintow.Insert(0, Sprint.Daj(backlogPelny.Backlog.SprintId));
            listaSprintow.Remove(null);

            // pobranie listy użytkowników
            List<Uzytkownik> listaUzytkownikow = aktualnyProjekt.DajUzytkownikowProjektu(idProjektu);
            listaUzytkownikow.Insert(0, new Uzytkownik() { Email = "     " });
            if(backlogPelny.Backlog != null)
                listaUzytkownikow.Insert(0, Uzytkownik.DajUzytkownika(backlogPelny.Backlog.PrzypisanyDoId));
            listaUzytkownikow.Remove(null);

            // stany do SelectList
            backlogPelny.Stany = Stan.DajStanyBacklogow();
            if(backlogPelny.Backlog != null)
                backlogPelny.Stany.Insert(0, Stan.DajStan(backlogPelny.Backlog.StanId));
            backlogPelny.Stany.Remove(null);

            // typy do SelectList
            backlogPelny.Typy = TypBacklogu.DajTypyBacklogow();
            if(backlogPelny.Backlog != null)
                backlogPelny.Typy.Insert(0, TypBacklogu.Daj(backlogPelny.Backlog.TypId));
            backlogPelny.Typy.Remove(null);

            // sprinty i użytkownicy do SelectList
            backlogPelny.Sprinty = listaSprintow;
            backlogPelny.Uzytkownicy = listaUzytkownikow;

            // widok częściowy - okienko
            return PartialView("_DodajEdytuj", backlogPelny);
        }

        [HttpPost]
        public ActionResult DodajEdytuj(DaneBacklogu backlogProjektu, string stan, string sprint, string typ, string uzytkownik)
        {
            bool czyEdycja = true;

            backlogProjektu.Backlog.StanId = Int32.Parse(stan);
            backlogProjektu.Backlog.SprintId = Int32.Parse(sprint);
            backlogProjektu.Backlog.TypId = Int32.Parse(typ);
            backlogProjektu.Backlog.PrzypisanyDoId = Int32.Parse(uzytkownik);
            //backlogProjektu.Backlog.ProjektId = backlogProjektu.ProjektId;

            if (ModelState.ContainsKey("Backlog.Id"))
                ModelState["Backlog.Id"].Errors.Clear();


            if(backlogProjektu.Backlog.ProjektId == 0)
            {
                czyEdycja = false;
                backlogProjektu.Backlog.ProjektId = backlogProjektu.ProjektId;

                if (ModelState.ContainsKey("Backlog.ProjektId"))
                    ModelState["Backlog.ProjektId"].Errors.Clear();
            }

            if (ModelState.IsValid)
            {
                using (var db = new ProjectMasterEntities())
                {
                    if (czyEdycja)
                        db.Entry<Backlog>(backlogProjektu.Backlog).State = EntityState.Modified;
                    else
                        db.Backlogs.Add(backlogProjektu.Backlog);

                    db.SaveChanges();
                }
            }

            // jeżeli ktoś wybierze pusty sprint
            if(backlogProjektu.Backlog.SprintId == 0)
            {
                // pobranie projektu
                Projekt projekt = Projekt.Daj(backlogProjektu.Backlog.ProjektId);

                // przykładowy sprint projektu
                Sprint spr = projekt.DajSprinty()[0];

                // przekieruj do wszystkich backlogów projektu
                return RedirectToAction("Index", "Sprint", new { @id = spr.Id, @idProjektu = projekt.Id });
            }

            return RedirectToAction("Index", "Sprint", new { @id = backlogProjektu.Backlog.SprintId });
            
        }
    }
}