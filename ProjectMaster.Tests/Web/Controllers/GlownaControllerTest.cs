﻿using System;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectMaster.Web.Controllers;

namespace ProjectMaster.Tests.Web.Controllers
{
    [TestClass]
    public class GlownaControllerTest
    {
        [TestMethod]
        public void IndexTest()
        {
            // Inicjacja
            GlownaController controller = new GlownaController();
            
            // Akt
            ViewResult rezultat = controller.Index() as ViewResult;

            // Asercje
            Assert.IsNotNull(rezultat);
            Assert.AreEqual("Strona startowa", rezultat.ViewBag.Message, "Błąd przesyłania danych");
        }
    }
}
