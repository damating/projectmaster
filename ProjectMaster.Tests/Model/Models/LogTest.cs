﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ProjectMaster.Tests.Model.Models
{
    [TestClass]
    public class LogTest
    {
        [TestMethod]
        public void OdczytajSciezkeTest()
        {
            string sciezka = System.Configuration.ConfigurationManager.AppSettings["sciezkaLogowania"];

            // jak jest inna ścieżka to trzeba podmienić
            Assert.AreEqual(sciezka, "I:/PK/SemestrV/Technologia.NET/Projekt/ProjectMaster/Logs/Test.log");
            //Assert.AreEqual(sciezka, "C:/Users/Paula/Source/Workspaces/ProjectMaster/Logs/Test.log");
        }
    }
}
