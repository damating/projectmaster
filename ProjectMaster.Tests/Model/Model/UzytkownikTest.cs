﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectMaster.Model;

namespace ProjectMaster.Tests.Model.Model
{
    [TestClass]
    public class UzytkownikTest
    {
        [TestMethod]
        public void SprawdzUzytkownikaTest()
        {
            Uzytkownik u = new Uzytkownik() { Email = "pg@pg.pl", Haslo = "pawel123" };

            Assert.IsTrue(u.SprawdzUzytkownika(), "Nie wyszukało poprawnego użytkownika");

            u.Email = "zly@uzytkownik.pl"; u.Haslo = "zlyUzytkownik";
            Assert.IsFalse(u.SprawdzUzytkownika(), "Wyszukano użytkownika, którego nie ma w bazie");
        }

        [TestMethod]
        public void DodajUzytkownika()
        {
            Uzytkownik u = new Uzytkownik() { Imie = "ImieTest", Nazwisko = "NazwiskoTest", Email = "test@test.test", Haslo = "testowy123" };

            Assert.IsTrue(u.DodajUzytkownika());
        }
    }
}
