﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectMaster.Model.Security;

namespace ProjectMaster.Tests.Security
{
    /// <summary>
    /// Testy dla klasy MD5Walidator w module Security
    /// </summary>
    [TestClass]
    public class MD5WalidatorTest
    {
        MD5Walidator md5 = new MD5Walidator();

        /// <summary>
        /// Test sprawdzający poprawność metody DajHashCode()
        /// </summary>
        [TestMethod]
        public void DajHashCodeTest()
        {
            string zrodlo = "Paula ma kota";
            string hash = md5.DajHashCode(zrodlo);
            string oczekiwanyHash = "803625a2edf491ec37f2ee44d5c74473";
            
            // Asserty
            Assert.AreEqual(hash, oczekiwanyHash, "Błędna konwersja tekstu");
        }

        /// <summary>
        /// Test sprawdzający poprawność metody Waliduj()
        /// </summary>
        [TestMethod]
        public void WalidujTest()
        {
            string zrodlo = "Paula ma kota";
            string hash = "803625a2edf491ec37f2ee44d5c74473";

            bool poprawne = md5.Waliduj(zrodlo, hash);

            Assert.IsTrue(poprawne, "Błędna walidacja");
        }
    }
}
