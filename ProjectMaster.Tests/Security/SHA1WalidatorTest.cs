﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectMaster.Model.Security;

namespace ProjectMaster.Tests.Security
{
    /// <summary>
    /// Testy dla klasy SHA1Walidator w module Security
    /// </summary>
    [TestClass]
    public class SHA1WalidatorTest
    {
        SHA1Walidator sha1 = new SHA1Walidator();

        /// <summary>
        /// Test sprawdzający poprawność metody DajHashCode()
        /// </summary>
        [TestMethod]
        public void DajHashCodeTest()
        {
            string zrodlo = "Paula ma kota";
            string hash = sha1.DajHashCode(zrodlo);
            string oczekiwanyHash = "7ce574e049ccd9a7703aede4c654ca740b7e4c19";

            // Asserty
            Assert.AreEqual(hash, oczekiwanyHash, "Błędna konwersja tekstu");
        }

        /// <summary>
        /// Test sprawdzający poprawność metody Waliduj()
        /// </summary>
        [TestMethod]
        public void WalidujTest()
        {
            string zrodlo = "Paula ma kota";
            string hash = "7ce574e049ccd9a7703aede4c654ca740b7e4c19";

            bool poprawne = sha1.Waliduj(zrodlo, hash);

            Assert.IsTrue(poprawne, "Błędna walidacja");
        }
    }
}
